import React, { useState } from "react";
import {
  Header,
  HeaderName,
  HeaderGlobalBar,
  HeaderGlobalAction,
  Search
} from "carbon-components-react";
//@ts-ignore
import { Search20, Location20, AppSwitcher20 } from "@carbon/icons-react";
import { Marker, GoogleMap } from "react-google-maps";

import "./App.css";
import "carbon-components/css/carbon-components.min.css";

import Place from "./interfaces/place";
import LatLngPosition from "./interfaces/position";

import { getLocationByString } from "./api/location";
import { getPlacesByLocation } from "./api/places";
import { sendBookingRequest } from "./api/booking";

import BookingDialog from "./components/BookingDialog/BookingDialog";
import PropertyMap from "./components/PropertyMap/PropertyMap";
import PropertyTile from "./components/PropertyTile/PropertyTile";
import LoadingDialog from "./components/LoadingDialog/LoadingDialog";
import FeedbackDialog from "./components/FeedbackDialog/FeedbackDialog";

interface BookingStatus {
  open: boolean;
  state: "entering" | "loading" | "done" | "error";
  id?: string;
  propertyTitle?: string;
}

const App: React.FC = () => {
  const [searchText, setSearchText] = useState("");
  const [places, setPlaces] = useState<Place[]>([]);
  const [bookingStatus, setBookingStatus] = useState<BookingStatus>({
    open: false,
    state: "entering",
    id: undefined,
    propertyTitle: undefined
  });
  let map: GoogleMap | null = null;

  const onSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchText(e.target.value);
  };

  const updatePlaces = async (position: LatLngPosition) => {
    const places = await getPlacesByLocation(position);
    setPlaces(places);
  };

  const onSearch = async () => {
    const position = await getLocationByString(searchText);
    if (map != null) {
      map.panTo(position);
    }
    updatePlaces(position);
  };

  const onLocate = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(data => {
        const position: LatLngPosition = {
          lat: data.coords.latitude,
          lng: data.coords.longitude
        };
        if (map != null) {
          map.panTo(position);
        }
        updatePlaces(position);
      });
    } else {
      window.alert("geolocation not supported");
    }
  };

  return (
    <div className="App">
      <Header aria-label="Limehome Challenge">
        <HeaderName href="#" prefix="Limehome">
          [Challenge]
        </HeaderName>
        <HeaderGlobalBar>
          <Search onChange={onSearchChange} labelText="City or Location" />
          <HeaderGlobalAction aria-label="Search" onClick={onSearch}>
            <Search20 />
          </HeaderGlobalAction>
          <HeaderGlobalAction aria-label="Locate Me" onClick={onLocate}>
            <Location20 />
          </HeaderGlobalAction>
          <HeaderGlobalAction aria-label="App Switcher" onClick={() => {}}>
            <AppSwitcher20 />
          </HeaderGlobalAction>
        </HeaderGlobalBar>
      </Header>
      <div id="content">
        <div id="map">
          <PropertyMap
            googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyB76gVOtvOCKYr0Eo8ER3SSJ13M2v76fsM&v=3.exp&libraries=geometry,drawing,places"
            mapRef={ref => {
              map = ref;
            }}
            loadingElement={<div style={{ height: `100%` }} />}
            containerElement={<div style={{ height: `100%` }} />}
            mapElement={<div style={{ height: `100%` }} />}
          >
            {places.map((place, index) => (
              <Marker
                key={place.id}
                label={String(index)}
                position={{
                  lat: place.position[0],
                  lng: place.position[1]
                }}
              ></Marker>
            ))}
          </PropertyMap>
        </div>
        <div id="list">
          <p>{places.length} potential locations found</p>
          {places.map((place, index) => (
            <PropertyTile
              key={place.id}
              title={`${index}) ${place.title}`}
              price={Math.floor(Math.random() * 112) + 30}
              starCount={(Math.floor(Math.random() * 30) + 20) / 10}
              id={place.id}
              onBookNowClick={props => {
                setBookingStatus({
                  ...bookingStatus,
                  id: props.id,
                  propertyTitle: props.title,
                  open: true,
                  state: "entering"
                });
              }}
              onDetailsClick={() => {
                window.alert("Details clicked");
              }}
            />
          ))}
        </div>
      </div>
      {bookingStatus.open && bookingStatus.state === "entering" && (
        <BookingDialog
          key={bookingStatus.id}
          propertyTitle={bookingStatus.propertyTitle}
          id={bookingStatus.id}
          open={bookingStatus.open}
          onRequestClose={() => {
            setBookingStatus({
              ...bookingStatus,
              open: false
            });
          }}
          onRequestSubmit={async (props, formState) => {
            if (
              props.id != null &&
              formState.startDate != null &&
              formState.endDate != null
            ) {
              setBookingStatus(bookingStatus => ({
                ...bookingStatus,
                state: "loading"
              }));
              const res = await sendBookingRequest({
                firstName: formState.firstName,
                lastName: formState.lastName,
                email: formState.email,
                startDate: formState.startDate,
                endDate: formState.endDate,
                propertyId: props.id
              });
              if (res.status === 201) {
                setBookingStatus(bookingStatus => ({
                  ...bookingStatus,
                  state: "done"
                }));
              } else {
                setBookingStatus(bookingStatus => ({
                  ...bookingStatus,
                  state: "error"
                }));
              }
            }
          }}
        />
      )}
      {bookingStatus.open && bookingStatus.state === "loading" && (
        <LoadingDialog />
      )}
      {bookingStatus.open && bookingStatus.state === "done" && (
        <FeedbackDialog
          onRequestClose={() => {
            setBookingStatus({
              ...bookingStatus,
              open: false
            });
          }}
        >
          Booking succesfully created
        </FeedbackDialog>
      )}
      {bookingStatus.open && bookingStatus.state === "error" && (
        <FeedbackDialog
          onRequestClose={() => {
            setBookingStatus({
              ...bookingStatus,
              open: false
            });
          }}
        >
          Error occured during the booking
        </FeedbackDialog>
      )}
    </div>
  );
};

export default App;
