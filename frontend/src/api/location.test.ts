import { getLocationByString } from "./location";

it("should return location", async () => {
  const location = await getLocationByString("Munich");
  expect(location).toBeDefined();
  expect(location.lat).not.toBe(0);
  expect(location.lng).not.toBe(0);
});

it("should return location 0,0 (if undefined location)", async () => {
  const location = await getLocationByString("ajksdkbakjsdbkasd");
  expect(location).toBeDefined();
  expect(location.lat).toBe(0);
  expect(location.lng).toBe(0);
});
