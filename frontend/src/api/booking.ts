import axios from "axios";

interface IBookingInformation {
  firstName: string;
  lastName: string;
  email: string;
  propertyId: string;
  startDate: Date;
  endDate: Date;
}

const sendBookingRequest = async (bookingInformation: IBookingInformation) => {
  return await axios.post("http://localhost:5000/booking/create", bookingInformation);
};

export { sendBookingRequest };
