import { getPlacesByLocation } from "./places";

it("should return places", async () => {
  const places = await getPlacesByLocation({ lat: 20, lng: 30 });
  expect(places).toBeDefined();
  expect(places.length).toBeGreaterThanOrEqual(0)
});
