import LatLngPosition from "../interfaces/position";
import axios from "axios";

const getLocationByString = async (
  locationString: string
): Promise<LatLngPosition> => {
  const res = await axios.get(
    `https://geocoder.api.here.com/6.2/geocode.json?app_id=Bje3ceZtLma95NVOGatt&app_code=168RVOt2CmRbhw3BQ2kDJQ&searchtext=${locationString}`
  );
  if (
    res.data.Response.View.length > 0 &&
    res.data.Response.View[0].Result.length > 0
  ) {
    const position =
      res.data.Response.View[0].Result[0].Location.DisplayPosition;
    return {
      lat: position.Latitude,
      lng: position.Longitude
    };
  }
  else {
    return {
      lat: 0,
      lng: 0,
    }
  }
};

export { getLocationByString };
