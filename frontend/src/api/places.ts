import LatLngPosition from "../interfaces/position";
import axios from "axios";
import Place from "../interfaces/place";

interface Response {
    results: Place[];
}

const getPlacesByLocation = async (location: LatLngPosition) => {
  const res = await axios.get<Response>(
    `https://places.cit.api.here.com/places/v1/autosuggest?at=${location.lat},${location.lng}&app_id=Bje3ceZtLma95NVOGatt&app_code=168RVOt2CmRbhw3BQ2kDJQ&q=Hotel`
  );
  return res.data.results.filter(result => result.resultType === "place" && result.category === "hotel");
};

export { getPlacesByLocation };
