interface Place {
  id: string;
  title: string;
  position: number[];
  category: string;
  resultType: string;
}

export default Place;
