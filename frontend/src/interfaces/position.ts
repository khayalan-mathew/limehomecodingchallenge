interface LatLngPosition {
    lat: number;
    lng: number;
}

export default LatLngPosition;