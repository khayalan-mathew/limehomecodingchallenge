import React from "react";
import { render } from "enzyme";
import PropertyTile from "./PropertyTile";

it("renders correctly", () => {
  const tree = render(<PropertyTile starCount={3} title="Test" price={50} id="tests" />)
  expect(tree).toMatchSnapshot();
});
