import { Tile, Button } from "carbon-components-react";
import React from "react";

import "./PropertyTile.css";

interface PropertyTileProps {
  title: string;
  starCount: number;
  price: number;
  id: string;
  imageSrc?: string;
  onBookNowClick?: (props: PropertyTileProps) => void;
  onDetailsClick?: (props: PropertyTileProps) => void;
}

const PropertyTile: React.FC<PropertyTileProps> = props => (
  <Tile
    className="propertyTile"
  >
    <div
      style={{
        flex: 0.3,
        backgroundColor: "rgba(0, 0, 0, 0.5)",
        backgroundImage: `url(${props.imageSrc})`
      }}
    ></div>
    <div style={{ flex: 0.7, display: "flex", flexDirection: "column" }}>
      <div style={{ padding: "1rem", flex: 1 }}>
        <p style={{ fontWeight: 600, fontSize: "22" }}>{props.title}</p>
        <p>
          {props.price}$/night | {props.starCount}/5 Stars
        </p>
      </div>
      <div style={{ display: "flex " }}>
        <Button kind="secondary" style={{ flex: 0.5 }} onClick={() => {
            if (props.onDetailsClick) {
              props.onDetailsClick(props);
            }
          }}>
          Details
        </Button>
        <Button
          onClick={() => {
            if (props.onBookNowClick) {
              props.onBookNowClick(props);
            }
          }}
          style={{ flex: 0.5 }}
        >
          Book now
        </Button>
      </div>
    </div>
  </Tile>
);

export default PropertyTile;
