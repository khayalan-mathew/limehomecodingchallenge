import React from "react";
import { render } from "enzyme";
import BookingDialog from "./BookingDialog";

it("renders correctly", () => {
  const tree = render(<BookingDialog propertyTitle="Test" open={true}/>)
  expect(tree).toMatchSnapshot();
});
