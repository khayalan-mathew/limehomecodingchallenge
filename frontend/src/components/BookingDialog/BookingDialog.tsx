import React, { useState } from "react";
import {
  Modal,
  Form,
  FormItem,
  TextInput,
  DatePickerInput,
  DatePicker
} from "carbon-components-react";
import * as EmailValidator from 'email-validator';

import "./BookingDialog.css";

interface BookingDialogProps {
  open?: boolean;
  propertyTitle?: string;
  id?: string;
  onRequestClose?: () => void;
  onRequestSubmit?: (props: BookingDialogProps, form: FormState) => void;
}

interface FormState {
  firstName: string;
  lastName: string;
  email: string;
  startDate: Date | null;
  endDate: Date | null;
}

// TODO: Add Form touch detection (maybe use Formik)

// TODO: Fix visual bug because of css grid and error display

const BookingDialog: React.FC<BookingDialogProps> = props => {
  const [form, setForm] = useState<FormState>({
    firstName: "",
    lastName: "",
    email: "",
    startDate: null,
    endDate: null
  });

  const isCurrentValid = (): boolean => {
    if(form.firstName.length < 2 || form.firstName.length > 20) {
      return false;
    }
    if(form.lastName.length < 2 || form.lastName.length > 20) {
      return false;
    }
    if(!EmailValidator.validate(form.email)) {
      return false;
    }
    if(form.startDate == null || form.endDate == null) {
      return false;
    }
    return true;
  }

  return (
    <Modal
      open={props.open}
      onRequestClose={props.onRequestClose}
      onRequestSubmit={() => {
        if (props.onRequestSubmit && isCurrentValid()) {
          props.onRequestSubmit(props, form);
        }
      }}
      modalHeading="Book now"
      modalLabel={props.propertyTitle}
      primaryButtonText="Book now"
      primaryButtonDisabled={!isCurrentValid()}
      secondaryButtonText="Cancel"
    >
      <Form className="parent">
        <FormItem className="div1">
          <TextInput
            id="firstName"
            key="firstName"
            labelText="First Name"
            onChange={e => {
              e.preventDefault();
              e.persist();
              setForm(form => ({
                ...form,
                firstName: e.target.value
              }));
            }}
            invalid={form.firstName.length < 2 || form.firstName.length > 20}
            invalidText="First name needs to be between 2 and 20 characters"
          />
        </FormItem>
        <FormItem className="div2">
          <TextInput
            id="lastName"
            labelText="Last Name"
            value={form.lastName}
            onChange={e => {
              e.preventDefault();
              e.persist();
              setForm(form => ({
                ...form,
                lastName: e.target.value
              }));
            }}
            invalid={form.lastName.length < 2 || form.lastName.length > 20}
            invalidText="Last name needs to be between 2 and 20 characters"
          />
        </FormItem>
        <FormItem className="div3">
          <TextInput
            id="email"
            labelText="Email"
            value={form.email}
            onChange={e => {
              e.preventDefault();
              e.persist();
              setForm(form => ({
                ...form,
                email: e.target.value
              }));
            }}
            invalid={!EmailValidator.validate(form.email)}
            invalidText="Email needs to be valid email address"
          />
        </FormItem>
        <FormItem className="div4">
          <DatePicker
            datePickerType="range"
            onChange={dates => {
              setForm(form => ({
                ...form,
                startDate: dates[0],
                endDate: dates[1]
              }));
            }}
          >
            <DatePickerInput
              labelText="Start date"
              style={{ backgroundColor: "#fff" }}
              id="date-picker-input-id-start"
            />
            <DatePickerInput
              labelText="End date"
              style={{ backgroundColor: "#fff" }}
              id="date-picker-input-id-end"
            />
          </DatePicker>
        </FormItem>
      </Form>
    </Modal>
  );
};

export default BookingDialog;
