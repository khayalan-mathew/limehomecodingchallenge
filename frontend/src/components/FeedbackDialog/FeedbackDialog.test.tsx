import React from "react";
import { render } from "enzyme";
import FeedbackDialog from "./FeedbackDialog";

it("renders correctly", () => {
  const tree = render(<FeedbackDialog/>)
  expect(tree).toMatchSnapshot();
});
