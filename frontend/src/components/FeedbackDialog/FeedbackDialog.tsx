import React from "react";
import { Modal } from "carbon-components-react";

interface BookingDialogProps {
  onRequestClose?: () => void;
}

const FeedbackDialog: React.FC<BookingDialogProps> = (props) => {
  return (
    <Modal
      modalAriaLabel="loadingModal"
      modalLabel="loading"
      passiveModal={true}
      open={true}
      onRequestClose={props.onRequestClose}
    >
      {props.children}
    </Modal>
  );
};

export default FeedbackDialog;
