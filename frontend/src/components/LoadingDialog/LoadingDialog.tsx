import React from "react";
import { Modal, Loading } from "carbon-components-react";

const LoadingDialog: React.FC = () => {
  return (
    <Modal modalAriaLabel="loadingModal" modalLabel="loading" passiveModal={true} open={true}>
      <div style={{ height: 200 }}>
        <Loading></Loading>
      </div>
    </Modal>
  );
};

export default LoadingDialog;
