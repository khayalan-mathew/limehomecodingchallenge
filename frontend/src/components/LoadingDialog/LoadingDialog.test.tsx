import React from "react";
import { render } from "enzyme";
import LoadingDialog from "./LoadingDialog";

it("renders correctly", () => {
  const tree = render(<LoadingDialog/>)
  expect(tree).toMatchSnapshot();
});
