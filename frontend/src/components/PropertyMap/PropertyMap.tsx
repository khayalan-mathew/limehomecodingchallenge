import React from "react";
import { GoogleMap, withGoogleMap, withScriptjs, WithGoogleMapProps } from "react-google-maps";

interface PropertyMapProps extends WithGoogleMapProps {
  mapRef?: (ref: GoogleMap | null) => void
}

const PropertyMap = withScriptjs<PropertyMapProps>(
  withGoogleMap(props => {
    return (
      <GoogleMap ref={props.mapRef} defaultZoom={13} defaultCenter={{ lat: -34.397, lng: 150.644 }}>
        {props.children}
      </GoogleMap>
    );
  })
);

export default PropertyMap;
