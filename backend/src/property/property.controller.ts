import { Controller, Get, Param } from '@nestjs/common';
import { BookingService } from '../booking/booking.service';
import { Booking } from '../booking/entities/booking.entity';

@Controller('property')
export class PropertyController {
  constructor(private readonly bookingService: BookingService) {}

  @Get(':id/bookings')
  async getBookingsByPropertyId(@Param('id') propertyId: string): Promise<{bookings: Booking[]}> {
    return {bookings: await this.bookingService.getBookingsByPropertyId(propertyId)};
  }
}
