import { Injectable } from '@nestjs/common';
import axios from 'axios';

@Injectable()
export class PropertyService {
  async propertyExists(propertyId: string): Promise<boolean> {
    try {
      const res = await axios.get(
        `https://places.cit.api.here.com/places/v1/places/lookup?app_id=Bje3ceZtLma95NVOGatt&app_code=168RVOt2CmRbhw3BQ2kDJQ&source=sharing&id=${propertyId}`,
      );
      return res.status === 200;
    } catch {
      return false;
    }
  }
}
