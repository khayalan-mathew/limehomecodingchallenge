export interface ICreateBooking {
    firstName: string;
    lastName: string;
    email: string;
    propertyId: string;
    startDate: Date;
    endDate: Date;
}
