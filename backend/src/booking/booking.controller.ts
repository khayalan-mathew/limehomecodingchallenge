import { Controller, Post, Body, HttpException } from '@nestjs/common';
import { BookingService } from './booking.service';
import { Booking } from './entities/booking.entity';
import { CreateBookingDto } from './dto/createbooking.dto';

@Controller('booking')
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}

  @Post('create')
  async createBooking(@Body() bookingData: CreateBookingDto): Promise<Booking> {
    try {
      return await this.bookingService.createBooking({
        ...bookingData,
        startDate: new Date(bookingData.startDate),
        endDate: new Date(bookingData.endDate),
      });
    } catch (e) {
      throw new HttpException(e.message, 400);
    }
  }
}
