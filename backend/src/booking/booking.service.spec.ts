import { Test, TestingModule } from '@nestjs/testing';
import { BookingService } from './booking.service';
import { Repository } from 'typeorm';
import { Booking } from './entities/booking.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserService } from '../user/user.service';
import { User } from '../user/entities/user.entity';
import { PropertyService } from '../property/property.service';

const templateBooking: Booking = {
  id: 1,
  firstName: 'test',
  lastName: 'test',
  startDate: new Date(),
  endDate: new Date(new Date(Date.now() + 24 * 60 * 60 * 1000)),
  propertyId: 'testing',
  user: undefined,
  userId: 1,
};

describe('BookingService', () => {
  let service: BookingService;
  let userService: UserService;
  let propertyService: PropertyService;
  let repository: Repository<Booking>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookingService,
        {
          provide: UserService,
          useValue: {
            findUserByEmail: () => {},
            createUser: () => {},
          },
        },
        {
          provide: PropertyService,
          useValue: {
            propertyExists: () => {},
          },
        },
        {
          // how you provide the injection token in a test instance
          provide: getRepositoryToken(Booking),
          // as a class value, Repository needs no generics
          useClass: Repository,
        },
      ],
    }).compile();

    service = module.get<BookingService>(BookingService);
    userService = module.get<UserService>(UserService);
    propertyService = module.get<PropertyService>(PropertyService);
    repository = module.get<Repository<Booking>>(getRepositoryToken(Booking));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should return bookings by user id', async () => {
    const testBooking: Booking = templateBooking;
    jest
      .spyOn(repository, 'find')
      .mockResolvedValueOnce(Promise.resolve([testBooking]));
    expect(await service.getBookingsByUserId(1)).toEqual([testBooking]);
  });

  it('should return bookings by property id', async () => {
    const testBooking: Booking = templateBooking;
    jest
      .spyOn(repository, 'find')
      .mockResolvedValueOnce(Promise.resolve([testBooking]));
    expect(await service.getBookingsByPropertyId('testing')).toEqual([
      testBooking,
    ]);
  });

  it('should create booking', async () => {
    const testBooking: Booking = templateBooking;
    const saveSpy = jest
      .spyOn(repository, 'save')
      .mockResolvedValueOnce(Promise.resolve(testBooking));
    const propertyExistsSpy = jest
      .spyOn(propertyService, 'propertyExists')
      .mockResolvedValueOnce(Promise.resolve(true));
    const userFindSpy = jest
      .spyOn(userService, 'findUserByEmail')
      .mockResolvedValueOnce(Promise.resolve(new User()));
    expect(
      await service.createBooking({ ...testBooking, email: 'test@test.com' }),
    ).toEqual(testBooking);
    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(propertyExistsSpy).toHaveBeenCalledTimes(1);
    expect(userFindSpy).toHaveBeenCalledTimes(1);
  });

  it('shouldnt create booking (reason: dates invalid)', async () => {
    const testBooking: Booking = {
      ...templateBooking,
      startDate: new Date(),
      endDate: new Date(),
    };
    expect(
      service.createBooking({ ...testBooking, email: 'test@test.com' }),
    ).rejects.toEqual(
      new Error(
        'Starting date has to be earlier than end date and atleast 1 day difference between',
      ),
    );
  });

  it('shouldnt create booking (reason: property Id invalid)', async () => {
    const testBooking: Booking = {
      ...templateBooking,
    };
    const propertyExistsSpy = jest
      .spyOn(propertyService, 'propertyExists')
      .mockResolvedValueOnce(Promise.resolve(false));
    expect(
      service.createBooking({ ...testBooking, email: 'test@test.com' }),
    ).rejects.toEqual(
      new Error(
        'Property does not exist',
      ),
    );
  });

  it('should create booking and user', async () => {
    const testBooking: Booking = templateBooking;
    const saveSpy = jest
      .spyOn(repository, 'save')
      .mockResolvedValueOnce(Promise.resolve(testBooking));
    const propertyExistsSpy = jest
      .spyOn(propertyService, 'propertyExists')
      .mockResolvedValueOnce(Promise.resolve(true));
    const userFindSpy = jest
      .spyOn(userService, 'findUserByEmail')
      .mockImplementation(() => {
        throw new Error('Entity not found');
      });
    const userCreateSpy = jest
      .spyOn(userService, 'createUser')
      .mockResolvedValueOnce(Promise.resolve(new User()));
    expect(
      await service.createBooking({ ...testBooking, email: 'test@test.com' }),
    ).toEqual(testBooking);
    expect(saveSpy).toHaveBeenCalledTimes(1);
    expect(propertyExistsSpy).toHaveBeenCalledTimes(1);
    expect(userFindSpy).toHaveBeenCalledTimes(1);
    expect(userCreateSpy).toHaveBeenCalledTimes(1);
  });
});
