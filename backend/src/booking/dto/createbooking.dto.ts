import { IsEmail, IsNotEmpty, Length, IsString, IsDateString } from 'class-validator';

export class CreateBookingDto {
  @IsEmail()
  email: string;

  @Length(2, 20)
  @IsString()
  firstName: string;

  @Length(2, 20)
  @IsString()
  lastName: string;

  @IsString()
  propertyId: string;

  @IsDateString()
  startDate: string;

  @IsDateString()
  endDate: string;
}
