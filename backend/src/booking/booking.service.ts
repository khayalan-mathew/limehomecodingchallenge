import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Booking } from './entities/booking.entity';
import { ICreateBooking } from './interfaces/createbooking.interface';
import { User } from '../user/entities/user.entity';
import { UserService } from '../user/user.service';
import { PropertyService } from '../property/property.service';

@Injectable()
export class BookingService {
  constructor(
    @InjectRepository(Booking)
    private readonly bookingRepository: Repository<Booking>,
    private readonly userService: UserService,
    private readonly propertyService: PropertyService,
  ) {}

  async createBooking(bookingInfo: ICreateBooking): Promise<Booking> {
    // TODO: Better way of checking same date (moment is overhead)
    // TODO: Move some logic from service into controller (better structure)
    const startDateBeforeEndDate =
      bookingInfo.startDate.valueOf() < bookingInfo.endDate.valueOf();
    const notSameDate =
      new Date(bookingInfo.startDate).toDateString() !==
      new Date(bookingInfo.endDate).toDateString();
    if (!(startDateBeforeEndDate && notSameDate)) {
      throw new Error(
        'Starting date has to be earlier than end date and atleast 1 day difference between',
      );
    }

    if (!(await this.propertyService.propertyExists(bookingInfo.propertyId))) {
      throw new Error('Property does not exist');
    }

    const booking = new Booking();
    let user: User;
    // Try to find user if it exists in database, if not create user with given email
    // Maybe moving this logic into Controller to seperate different concerns even further
    try {
      user = await this.userService.findUserByEmail(bookingInfo.email);
    } catch {
      user = await this.userService.createUser(bookingInfo.email);
    }
    booking.firstName = bookingInfo.firstName;
    booking.lastName = bookingInfo.lastName;

    booking.startDate = new Date(bookingInfo.startDate);
    booking.endDate = new Date(bookingInfo.endDate);

    booking.propertyId = bookingInfo.propertyId;

    booking.user = Promise.resolve(user);

    return this.bookingRepository.save(booking);
  }

  async getBookingsByPropertyId(propertyId: string): Promise<Booking[]> {
    return await this.bookingRepository.find({
      propertyId,
    });
  }

  async getBookingsByUserId(userId: number): Promise<Booking[]> {
    return await this.bookingRepository.find({
      userId,
    });
  }
}
