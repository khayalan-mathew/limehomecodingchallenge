import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async createUser(email: string): Promise<User> {
    const user = new User();
    user.email = email;
    return await this.userRepository.save(user);
  }

  async findUserById(id: number): Promise<User> {
    return await this.userRepository.findOneOrFail(id);
  }

  async findUserByEmail(email: string): Promise<User> {
    return await this.userRepository.findOneOrFail({ email });
  }
}
