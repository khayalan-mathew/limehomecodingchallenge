import { Controller, Get, Param } from '@nestjs/common';
import { Booking } from '../booking/entities/booking.entity';
import { BookingService } from '../booking/booking.service';

@Controller('user')
export class UserController {
  constructor(private readonly bookingService: BookingService) {}

  @Get(':id/bookings')
  async getBookingsByUserId(@Param('id') userId: number): Promise<{bookings: Booking[]}> {
    return { bookings: await this.bookingService.getBookingsByUserId(userId) };
  }
}
