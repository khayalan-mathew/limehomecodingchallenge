import { Test, TestingModule } from '@nestjs/testing';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('Basic e2e', () => {
  let app;
  const userEmail = 'test@test.com';
  const propertyId = 'testing';
  let userId: number;
  let bookingId: number;

  beforeAll(async () => {

    // Should initialize database and seed it

    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  // TODO: Clean up database afterwards

  it('/booking/create (right input) (POST)', async done => {
    const res = await request(app.getHttpServer())
      .post('/booking/create')
      .send({
        propertyId,
        email: userEmail,
        firstName: 'Test',
        lastName: 'Test',
        startDate: new Date(),
        endDate: new Date(Date.now() + 24 * 60 * 60 * 1000),
      });
    expect(res.status).toBe(201);
    expect(res.body.propertyId).toBe(propertyId);
    expect(res.body.__user__.email).toBe(userEmail);
    userId = res.body.__user__.id;
    bookingId = res.body.id;
    done();
  });

  it('/user/x/bookings (GET)', async done => {
    const res = await request(app.getHttpServer()).get(
      `/user/${userId}/bookings`,
    );
    expect(res.status).toBe(200);
    expect(res.body.bookings.find(booking => booking.id === bookingId)).toBeTruthy();
    done();
  });

  it('/property/x/bookings (GET)', async done => {
    const res = await request(app.getHttpServer()).get(
      `/property/${propertyId}/bookings`,
    );
    expect(res.status).toBe(200);
    expect(res.body.bookings.find(booking => booking.id === bookingId)).toBeTruthy();
    done();
  });
});
